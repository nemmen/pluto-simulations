Bondi accretion flow
======================

2D Bondi accretion flow simulation, following Ruffert et al. (1994). 

Model corresponds to their model B1.

Prepared by M. L. Gubolin.